COMPILER := gcc
STD 	 := c20
FLAGS 	 := -ggdb -O2 -DNDEBUG -pedantic-errors -Wall -Weffc++ -Wextra -Wsign-conversion -Werror

SRC := $(wildcard *.cpp)


all: compile run clean

compile: 
	@-mkdir out
	-$(COMPILER) $(SRC) -o out/main -std=$(STD) $(FLAGS)


run:
	@echo ""
	./out/main $(args)

clean:
	@echo ""
	rm -f out/*
